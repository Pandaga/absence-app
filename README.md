Absence App

## Structure
There are 3 catalogs; each contains one part of app. In `server` files for serveare. `adminPanel` and `userPanel` are two front-end apps for managers to check absences and user (managers & employees both) to report absences.

## Booting
Server: in `server` catalog run `node server.js`

Admin Panel & Users Panel: in `adminPanel` (or `usersPanel`) run `ng serve`. Those front-end apps cannot run simultaneously
