import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LogInComponent } from './paths/log-in/log-in.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { FindInfoComponent } from './paths/find-info/find-info.component';
import {
  MatDatepickerModule,
  MatNativeDateModule,
  MatFormFieldModule,
  MatInputModule,
  MatDividerModule,
  MatSelectModule,
  MatTabsModule,
  MatListModule,
  MatTableModule
} from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ResultTableComponent } from './components/result-table/result-table.component';
import { CalendarComponent } from './components/calendar/calendar.component';
import { SelectionListComponent } from './components/selection-list/selection-list.component';
import { SubmitButtonComponent } from './components/submit-button/submit-button.component';
import { LogOutButtonComponent } from './components/log-out-button/log-out-button.component';

@NgModule({
  declarations: [
    AppComponent,
    LogInComponent,
    FindInfoComponent,
    ResultTableComponent,
    CalendarComponent,
    SelectionListComponent,
    SubmitButtonComponent,
    LogOutButtonComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    MatTableModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatFormFieldModule,
    MatInputModule,
    MatDividerModule,
    MatSelectModule,
    MatTabsModule,
    MatListModule,
    BrowserAnimationsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
