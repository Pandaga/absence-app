import { Component, Input } from '@angular/core';
import { FormGroup } from "@angular/forms";

@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.scss']
})
export class CalendarComponent {
  @Input() searchForm: FormGroup;
  @Input() controlName: string;
  @Input() placeholder: string;

  constructor() { }

}
