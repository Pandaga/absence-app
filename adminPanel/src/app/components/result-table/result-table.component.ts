import { Component, OnChanges, Input, ViewChild } from '@angular/core';

@Component({
  selector: 'app-result-table',
  templateUrl: './result-table.component.html',
  styleUrls: ['./result-table.component.scss']
})
export class ResultTableComponent implements OnChanges {

  /**
   * Search result from parent component
   */
  @Input() searchResult;

  /**
   * Formatted data from searchResult for result table
   */
  formattedData = [];

  /**
   * Columns for table
   */
  displayedColumns = ['login', 'date','absencePeriod', 'category','duration', 'description'];

  @ViewChild('table') table;

  /**
   * Categories of causes of absence
   */
  categories = [
    {key: 'work-home', value: 'Work at home'},
    {key: 'personal', value: 'Personal matters'},
    {key: 'doctor', value: 'Doctor\'s visit'}
  ];

  constructor() { }

  ngOnChanges() {
    if (this.searchResult) {
      this.formattedData = [];
      for (let index = 0; index < this.searchResult.length; index++) {
        let searchData = this.searchResult[index];
        let longCategory = '';
        let fromDate = new Date(searchData.fromDate);
        let creationDate = new Date(searchData.creationDate);

        for (let i = 0; i < this.categories.length; i++) {
          if (searchData.absenceCategory === this.categories[i].key) {
            longCategory = this.categories[i].value;
          }
        }

        if (searchData.fromDate === searchData.toDate) {
          this.formattedData.push({
            login: searchData.login,
            date: creationDate.getDate() + '/' + (creationDate.getMonth() + 1)+ '/' + creationDate.getFullYear(),
            absencePeriod: fromDate.getDate() + '/' + (fromDate.getMonth() + 1),
            absenceCategory: longCategory,
            duration: searchData.fromTime + ' - ' + searchData.toTime,
            additionalInfo: searchData.additionalInfo
          });
        } else {
          let toDate = new Date(searchData.toDate);
          this.formattedData.push({
            login: searchData.login,
            date: creationDate.getDate() + '/' + (creationDate.getMonth() + 1) + '/' + creationDate.getFullYear(),
            absencePeriod: fromDate.getDate() + '/' + (fromDate.getMonth() + 1) + ' - ' + toDate.getDate() + '/' + (toDate.getMonth() + 1),
            absenceCategory: longCategory,
            duration: this.differenceBetweenDates(searchData.fromDate, searchData.toDate) + ' day(s)',
            additionalInfo: searchData.additionalInfo
          });
        }
      }
    }
    if (this.table) {
      this.table.renderRows();
    }
  }

  /**
   * Compute difference in days between two dates
   * @param {Date} date1
   * @param {Date} date2
   */
  differenceBetweenDates(date1, date2) {
    let diff = Date.parse(date2) - Date.parse(date1);
    return Math.floor(diff / 86400000);
  }

}
