import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FindInfoComponent } from './find-info.component';

describe('FindInfoComponent', () => {
  let component: FindInfoComponent;
  let fixture: ComponentFixture<FindInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FindInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FindInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
