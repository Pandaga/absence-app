import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { StoreService } from "../../shared/store.service";

@Component({
  selector: 'app-find-info',
  templateUrl: './find-info.component.html',
  styleUrls: ['./find-info.component.scss']
})
export class FindInfoComponent implements OnInit {

  /**
   * Form with search criteria
   */
  searchForm: FormGroup;

  /**
   * All users for selection list
   */
  allUsers;

  /**
   * Results of search
   * @type {Object}
   */
  searchResult;

  constructor(private store: StoreService) { }

  ngOnInit() {
    this.searchForm = new FormGroup({
      users: new FormControl([]),
      fromDate: new FormControl(null),
      toDate: new FormControl(null)
    });
    this.store.getAllUsers().subscribe(response => {
      this.allUsers = response;
    });
  }

  onSubmit() {
    this.store.searchForResults(
      this.searchForm.get('users').value,
      this.searchForm.get('fromDate').value,
      this.searchForm.get('toDate').value
    ).subscribe(response => {
        this.searchResult = response;
      });
  }

}
