import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormControl} from '@angular/forms';
import { Router } from '@angular/router';
import { StoreService } from "../../shared/store.service";

@Component({
  selector: 'app-log-in',
  templateUrl: './log-in.component.html',
  styleUrls: ['./log-in.component.scss']
})
export class LogInComponent implements OnInit {
  submitted = false;
  signInForm: FormGroup;
  wrongCredentials = false;

  constructor(private router: Router,
              private store: StoreService) { }

  /**
   * On making component makes sigInForm for reactive forms
   */
  ngOnInit() {
    this.signInForm = new FormGroup({
      login: new FormControl('', Validators.required),
      password: new FormControl('', Validators.required)
    });
  }

  /**
   * Getter for login property of signInForm
   */
  get login() {
    return this.signInForm.get('login');
  }

  /**
   * Getter for password property of signInForm
   */
  get password() {
    return this.signInForm.get('password');
  }

  /**
   * Function activates when button is clicked & submit data to login function
   */
  onSubmit() {
    this.submitted = true;
    this.store.logInAdmin(this.login.value, this.password.value).subscribe(response => {
      if (response && response.id) {
        this.wrongCredentials = false;
        this.store.setUserId(response.id);
        this.router.navigate([`find-info`]);
      } else {
        this.wrongCredentials = true;
      }
    });
  };
}
