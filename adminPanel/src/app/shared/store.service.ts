import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class StoreService {
  /**
   * @type {string} Url of server
   */
  private url = 'http://localhost:8081';

  /**
   * @type {number} userId Id of user currently using app
   */
  private userId: number;

  constructor(private http: HttpClient) { }

  /**
   * Logs in managers
   * @param {string} login Login of manager
   * @param {string} password Manager's password
   */
  logInAdmin(login: string, password: string): Observable<any> {
    return this.http.post(this.url + '/login', { login, password, role: 'manager'});
  }

  /**
   * Deletes userId
   */
  logOutUser() {
    this.setUserId(null);
  }

  /**
   * Sets user ID after log in
   * @param {number} userId Id of user
   */
  setUserId(userId: number) {
    this.userId = userId;
  }

  /**
   * Gets all users of app
   */
  getAllUsers() {
    return this.http.get(this.url + '/users');
  }

  /**
   * Sends data to server for search
   * @param {Object} users Chosen user
   * @param {Date} fromDate Date from which searching should start
   * @param {Date} toDate Date to which searching should be conducted
   */
  searchForResults(users, fromDate, toDate) {
    let userIds = [];
    for (let index in users) {
      userIds.push(users[index].userId);
    }
    return this.http.post(this.url + `/search`, {users: userIds, fromDate, toDate});
  }
}
