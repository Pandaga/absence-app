const express = require('express');
const app = express();
const cors = require('cors');
const fs = require('fs');

let corsOptions = {
   origin: 'http://localhost:4200',
   optionsSuccessStatus: 200
};
app.use(cors(corsOptions));

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.set('userData', require('./userData.json'));
app.set('absenceData', require('./absenceData.json'));

/**
 *  Route for signing in to users panel
 */
app.route('/login').post((req, res) => {
   let goodLogin = app.get('userData').users.find(user => {
      if (req.body.role) {
         return user.login === req.body.login
             && user.password === req.body.password
             && user.role === req.body.role;
      } else {
         return user.login === req.body.login
             && user.password === req.body.password;
      }
   });

   res.send({id: goodLogin ? goodLogin.id : null});
});

/**
 * Route for getting all users of app
 */
app.route('/users').get((req, res) => {
   let allUsers = [];
   app.get('userData').users.forEach(user => {
      allUsers.push({
         userId: user.id,
         login: user.login,
         name: user.name,
         role: user.role
      });
   });

   res.send(JSON.stringify(allUsers));
});

/**
 * Route for posting absence
 */
app.route('/absence').post((req, res) => {
   let absenceData = require('./absenceData.json');
   absenceData.data.push(JSON.parse(req.body.absenceData));
   fs.writeFile('absenceData.json', JSON.stringify(absenceData, null, 2), 'utf8', (err => {
      if (err) {
         console.log(err);
      } else {
         res.send(true);
      }
   }));
});

/**
 * Route for search for specific records of absence
 */
app.route('/search').post((req, res) => {
   let searchResult = [];
   let userName = '';

   let dateFrom = req.body.fromDate ? new Date(req.body.fromDate) : new Date(new Date().setFullYear(new Date().getFullYear() - 1));
   let dateTo = req.body.toDate ? new Date(req.body.toDate) : new Date(new Date().setFullYear(new Date().getFullYear() + 1));

   app.get('absenceData').data.forEach(absence => {
      let absenceFrom = new Date(absence.fromDate);
      let absenceTo = new Date(absence.toDate);
      if (req.body.users.length !== 0) {
         req.body.users.forEach(userId => {
            app.get('userData').users.forEach(user => {
               if (user.id === userId
                   && userId === absence.userId
                   && dateFrom.getTime() <= absenceFrom.getTime()
                   && dateTo.getTime() >= absenceTo.getTime()) {
                  searchResult.push({
                     login: user.name,
                     creationDate: absence.creationDate,
                     fromDate: absence.fromDate,
                     toDate: absence.toDate,
                     fromTime: absence.fromTime,
                     toTime: absence.toTime,
                     absenceCategory: absence.absenceCategory,
                     additionalInfo: absence.additionalInfo
                  });
               }
            });
         });
      } else {
         userName = app.get('userData').users.find(user => {
            return user.id === absence.userId;
         });
         if (dateFrom.getTime() <= absenceFrom.getTime()
             && dateTo.getTime() >= absenceTo.getTime()) {
            searchResult.push({
               login: userName.name,
               creationDate: absence.creationDate,
               fromDate: absence.fromDate,
               toDate: absence.toDate,
               fromTime: absence.fromTime,
               toTime: absence.toTime,
               absenceCategory: absence.absenceCategory,
               additionalInfo: absence.additionalInfo
            });
         }
      }
   });

   res.send(searchResult);
});

const server = app.listen(8081, function () {
   const host = server.address().address;
   const port = server.address().port;

   console.log("Example app listening at http://%s:%s", host, port)
});
