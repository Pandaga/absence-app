import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LogInComponent } from './paths/log-in/log-in.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { StoreService } from "./shared/store.service";
import { HttpClientModule } from '@angular/common/http';
import { ReportComponent } from './paths/report/report.component';
import { CalendarComponent } from "./components/calendar/calendar.component";
import {
  MatDatepickerModule,
  MatNativeDateModule,
  MatFormFieldModule,
  MatInputModule,
  MatDividerModule,
  MatSelectModule,
  MatTabsModule,
  MatListModule
} from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TimePickerComponent } from './components/time-picker/time-picker.component';
import { SelectInputComponent } from './components/select-input/select-input.component';
import { SubmitButtonComponent } from './components/submit-button/submit-button.component';
import { LogOutButtonComponent } from './components/log-out-button/log-out-button.component';

@NgModule({
  declarations: [
    AppComponent,
    LogInComponent,
    CalendarComponent,
    ReportComponent,
    TimePickerComponent,
    SelectInputComponent,
    SubmitButtonComponent,
    LogOutButtonComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatFormFieldModule,
    MatInputModule,
    MatDividerModule,
    BrowserAnimationsModule,
    MatSelectModule,
    MatTabsModule,
    MatListModule
  ],
  providers: [
    StoreService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
