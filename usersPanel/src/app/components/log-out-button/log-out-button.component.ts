import { Component, Input } from '@angular/core';
import { StoreService } from "../../shared/store.service";
import { Router } from '@angular/router';

@Component({
  selector: 'app-log-out-button',
  templateUrl: './log-out-button.component.html',
  styleUrls: ['./log-out-button.component.scss']
})
export class LogOutButtonComponent {

  @Input() text: string;

  constructor(private store: StoreService,
              private router: Router) { }

  /**
   * Logs out user
   */
  logOut() {
    this.store.logOutUser();
    this.router.navigate([`log-in`]);
  }
}
