import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { StoreService } from "../../shared/store.service";
import { Router } from '@angular/router';

@Component({
  selector: 'app-log-in',
  templateUrl: './log-in.component.html',
  styleUrls: ['./log-in.component.scss']
})
export class LogInComponent implements OnInit {

  /**
   * Variable for when user decides to submit data
   * @type {boolean}
   */
  submitted = false;

  /**
   * Form for signing in to app
   */
  signInForm: FormGroup;

  /**
   * Variable for showing message if credentials are wrong
   */
  wrongCredentials = false;

  constructor(private store: StoreService,
              private router: Router) { }

  ngOnInit() {
    this.signInForm = new FormGroup({
      login: new FormControl('', Validators.required),
      password: new FormControl('', Validators.required)
    });
  }

  /**
   * Getter for login property of signInForm
   */
  get login() {
    return this.signInForm.get('login');
  }

  /**
   * Getter for password property of signInForm
   */
  get password() {
    return this.signInForm.get('password');
  }

  /**
   * Function activates when button is clicked & submit data to login function
   */
  onSubmit() {
    this.submitted = true;
    this.store.logInUser(this.login.value, this.password.value).subscribe((response) => {
      if (response) {
        this.wrongCredentials = false;
        this.store.setUserId(response.id);
        this.router.navigate([`report`]);
      } else {
        this.wrongCredentials = true;
      }
    });
  };
}
