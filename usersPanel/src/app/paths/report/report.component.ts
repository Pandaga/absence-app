import { Component, OnInit } from '@angular/core';
import { StoreService } from "../../shared/store.service";
import { FormControl, FormGroup, Validators } from "@angular/forms";

@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.scss']
})
export class ReportComponent implements OnInit {

  /**
   * Form for reporting absence
   */
  absenceForm: FormGroup;

  /**
   * Variable for when user decides to submit data
   * @type {boolean}
   */
  submitted = false;

  /**
   * Variable for when posting absence is successful
   * @type {boolean}
   */
  successPost = false;

  /**
   * Categories of causes of absence
   */
  categories = [
    {value: 'work-home', viewValue: 'Work at home'},
    {value: 'personal', viewValue: 'Personal matters'},
    {value: 'doctor', viewValue: 'Doctor\'s visit'}
  ];

  constructor(private store: StoreService) { }

  ngOnInit() {
    this.absenceForm = new FormGroup({
      fromDate: new FormControl(null, Validators.required),
      toDate: new FormControl(null, Validators.required),
      fromTime: new FormControl(null, [
        Validators.required,
        Validators.min(8),
        Validators.max(16)
      ]),
      toTime: new FormControl(null, [
        Validators.required,
        Validators.min(8),
        Validators.max(16)
      ]),
      absenceCategory: new FormControl(null, Validators.required),
      additionalInfo: new FormControl('')
    });
  }

  /**
   * Getter for date value of absenceForm
   */
  get fromDate() {
    return this.absenceForm.get('fromDate');
  }

  /**
   * Getter for date value of absenceForm
   */
  get toDate() {
    return this.absenceForm.get('toDate');
  }

  /**
   * Getter for fromTime value of absenceForm
   */
  get fromTime() {
    return this.absenceForm.get('fromTime');
  }

  /**
   * Getter for toTime value of absenceForm
   */
  get toTime() {
    return this.absenceForm.get('toTime');
  }

  /**
   * Getter for absenceCategory value of absenceForm
   */
  get absenceCategory() {
    return this.absenceForm.get('absenceCategory');
  }

  /**
   * Getter for additionalInfo value of absenceForm
   */
  get additionalInfo() {
    return this.absenceForm.get('additionalInfo');
  }

  /**
   * Submits data if all required values are right
   */
  onSubmit() {
    this.submitted = true;
    if (!this.fromDate.invalid
      && !this.toDate.invalid
      && !this.fromTime.invalid
      && !this.toTime.invalid
      && !this.absenceCategory.invalid) {

      const absenceData = JSON.stringify({
        userId: this.store.getUserId(),
        creationDate: new Date(),
        fromDate: this.fromDate.value,
        toDate: this.toDate.value,
        fromTime: this.fromTime.value,
        toTime: this.toTime.value,
        absenceCategory: this.absenceCategory.value,
        additionalInfo: this.additionalInfo.value
      });

      this.store.postUserAbsence(absenceData).subscribe(response => {
        this.successPost = response;
      });
    }
  }

}
