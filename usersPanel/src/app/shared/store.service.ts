import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class StoreService {
  /**
   * @type {string} Url of server
   */
  private url = 'http://localhost:8081';

  /**
   * @type {number} userId Id of user currently using app
   */
  private userId: number;

  constructor(private http: HttpClient) { }

  /**
   * Logs in user to app
   * @param {string} login Login of user
   * @param {string} password User's password
   */
  logInUser(login: string, password: string): Observable<any> {
    return this.http.post(this.url + '/login', { login, password });
  }

  /**
   * Deletes userId
   */
  logOutUser() {
    this.setUserId(null);
  }

  /**
   * Sets user ID after log in
   * @param {number} userId Id of user
   */
  setUserId(userId: number) {
    this.userId = userId;
  }

  /**
   * Gets userId
   */
  getUserId() {
    return this.userId;
  }

  /**
   * Sends data about user's absence to server
   * @param absenceData
   */
  postUserAbsence(absenceData): Observable<any> {
    return this.http.post(this.url + '/absence', { absenceData });
  }
}
